rdforms.spec({
  language: 'sv',
  introduction: '<p>Hepp</p>',
  bundles: [
    ['./dcterms.json'],
    ['./dc.json'],
    ['./foaf.json'],
    ['./schema.json'],
    ['./skr.json']
  ],
  main: [
    'skr:WorkOfArt',
    'skr:Artist',
    'skr:PostalAddress',
    'skr:PropertyUnit',
    'skr:Image'
  ],
  supportive: [
  ]
});